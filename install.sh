#!/usr/bin/env bash

command -v git >/dev/null 2>&1 || { echo "git is required.  Aborting." >&2; exit 1; }
command -v npm >/dev/null 2>&1 || { echo "npm is required.  Aborting." >&2; exit 1; }
command -v node >/dev/null 2>&1 || { echo "/!\\ You will need to install NodeJS for the tool to work. /!\\" >&2; }

git clone https://Jainaisisse@bitbucket.org/Jainaisisse/bak-off.git BaKOff \
&& cd BaKOff/ \
&& cp config/config.json.example config/config.json \
&& touch config/accounts.txt \
&& npm install \
&& echo -e "\n==> Now edit config/config.json and add accounts into config/accounts.txt." \
&& echo -e "==> When done, run ./run.sh to start the tool."
