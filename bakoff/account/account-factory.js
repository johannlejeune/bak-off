/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import Account from "./index";
import ES6Error from "es6-error";
import { readFile } from "fs-promise";

/**
 * An exception thrown whenever an error happens while parsing accounts file.
 * @extends ES6Error
 */
class MalformedAccountsFile extends ES6Error {
    /**
     * Constructs the error.
     * @param {string} message The message.
     */
    constructor(message) {
        super(message);
    }
}

/**
 * A factory that creates accounts.
 */
class AccountFactory {
    /**
     * Reads a file containing accounts and creates Account instances.
     * @param {string} fileName The file name.
     * @returns {Account[]} An array containing the created Account instances.
     *
     * Accounts stored in the file are expected to follow this syntax :
     * username:password
     */
    static async createAccountsFromFile(fileName) {
        const content = await readFile(fileName);
        const lines = content.toString().split("\n").filter((line) => !!line);
        const accounts = [];
        for (const line of lines) {
            const [username, password] = line.split(":");
            if (!username || !password) {
                throw new MalformedAccountsFile(`Line '${line}' is malformed.`);
            }
            accounts.push(new Account(username, password));
        }
        return accounts;
    }
}

export default AccountFactory;
