/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import "babel-polyfill";
import BaKOff from "./bakoff";
import { configureLogger } from "./bakoff/logger";
import * as constants from "./bakoff/constants";

import { getLogger } from "log4js";
const logger = getLogger();

let bakoffInstance = null;

// Catch exit to provide clean shutdown.
process.once("exit", (exitCode) => {
    logger.info("Exiting...");
    bakoffInstance.stop();
    // Exit process.
    process.exit(exitCode);
});

// Catch uncaught exceptions.
process.on("uncaughtException", (error) => {
    logger.fatal("Uncaught exception.", error);
    process.exit(1);
});

process.on("SIGINT", () => {
    // Emit the event so that it gets caught once.
    process.emit("exit", 0);
});

// Application entry point.
if (require.main === module) {
    configureLogger(constants.LOGS_PATH);

    logger.info("BaKOff (╯°□°）╯︵ ┻━┻");

    bakoffInstance = new BaKOff();
    // Initialize, then fire !
    bakoffInstance.initialize()
        .then(() => bakoffInstance.start())
        .catch((error) => logger.fatal(error));
}
